# -*- coding: utf-8 -*-
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.contrib.messages.views import SuccessMessageMixin

from marketapp.forms.cliente import ClienteForm
from marketapp.models.cliente import Cliente


class ClienteCreateView(SuccessMessageMixin, CreateView):
    """
     Adiciona um Cliente.
    :URl: http://ip_servidor/cliente/cadastrar/
    """
    template_name = "marketapp/cliente/cliente_form.html"
    model = Cliente
    form_class = ClienteForm
    success_message = "Usuario %(name)s cadastrado com sucesso!"

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
            name=self.object.username,
        )

    def get_success_url(self):
        return reverse_lazy('login')
