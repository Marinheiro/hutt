# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from marketapp.models.pedido import Pedido
from marketapp.permissions.decorator import is_superuser


@method_decorator(is_superuser, name='dispatch')
class PedidoListView(LoginRequiredMixin, ListView):
    '''
     Lista todos os Pedido.
    :URl: http://ip_servidor/pedido/listar/
    '''
    queryset = Pedido.objects.all()
    template_name = "marketapp/pedido/pedido_list_admin.html"
    paginate_by = 5
