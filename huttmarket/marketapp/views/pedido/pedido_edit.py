# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic import *
from django.contrib.messages.views import SuccessMessageMixin
from marketapp.forms.pedido import PedidoForm
from marketapp.models.pedido import Pedido


class AjaxPedidoUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    '''
     Atualiza um pedido.
    :URl: http://ip_servidor/pedido/<id>/atualizar
    '''
    template_name_json = 'includes/form_json.html'
    template_mensagem = 'includes/mensagem_json.html'

    def get(self, request, id, **kwargs):
        '''
        :param request:
        :param id: id do pedido
        :param kwargs:
        :return: o cadastro de uma atualização
        '''
        context = {}
        data = {}
        pedido = Pedido.objects.get(id=id)
        form = PedidoForm(id=pedido.produto.id, instance=pedido)
        context['form'] = form
        context['titulo'] = get_object_or_404(Pedido, pk=self.kwargs.get("id"))
        context['editado'] = True
        context['url'] = reverse('pedido-editar', kwargs={"id": id})
        data['html_form'] = render_to_string(self.template_name_json, context, request=request)
        return JsonResponse(data)

    def post(self, request, id):
        '''

        :param request:
        :param id: do pedido
        :return: retorna o sucesso ou não
        '''
        pedido = Pedido.objects.get(id=id)
        form = PedidoForm(request.POST, request.FILES, id=pedido.produto.id, instance=pedido)
        if form.is_valid():
            form = form.save(commit=False)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        '''
        :param form:
        :return: retorna retorna o sucesso do cadastro
        '''
        data = dict()
        context = {}
        form.save()
        data['form_is_valid'] = True
        data['editado'] = True
        context['editado'] = True
        pedido = Pedido.objects.get(id=self.kwargs.get("id"))
        data['pedido'] = pedido.id
        data['quantidade'] = pedido.quantidade
        data['preco'] = pedido.preco_unit
        data['preco_antigo'] = pedido.produto.preco_unit
        pedidos = Pedido.objects.filter(desativado=False, finalizado=False, cliente__username=self.request.user)
        data['total'] = sum(valor for valor in [pedido.quantidade * pedido.preco_unit for pedido in pedidos])
        data['html_mensagem'] = render_to_string(self.template_mensagem, context, request=self.request)
        return JsonResponse(data)

    def form_invalid(self, form):
        '''
        :param form:
        :return: retorna o erro no cadastro da atualização
        '''
        context = {}
        data = dict()
        data['form_is_valid'] = False
        context['form'] = form
        context['classe_css'] = 'pedido_editar'
        context['titulo'] = get_object_or_404(Pedido, pk=self.kwargs.get("id"))
        context["url"] = reverse("pedido-editar", kwargs={"id": self.kwargs.get("id")})
        data['html_form'] = render_to_string(self.template_name_json, context, request=self.request)
        return JsonResponse(data)
