# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import *
from django.contrib.messages.views import SuccessMessageMixin
from marketapp.models.pedido import Pedido


class PedidoCheckoutView(LoginRequiredMixin, SuccessMessageMixin, View):
    '''
     Comprar item isolado de um carrinho.
    :URl: http://ip_servidor/produto/<id>/comprar
    '''
    success_message = "Produto Comprado com sucesso!"

    def get(self, request, id, *args, **kwargs):
        object = Pedido.objects.get(pk=id)
        object.finalizado = True
        object.save()
        messages.success(self.request, self.success_message)
        return redirect(reverse('carrinho-list'))
