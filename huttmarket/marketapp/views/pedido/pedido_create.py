# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic import *
from marketapp.forms.pedido import PedidoForm
from marketapp.models.cliente import Cliente
from marketapp.models.pedido import Pedido
from marketapp.models.produto import Produto


class AjaxPedidoCreateView(View):
    '''
    Adiciona um pedido via AJAX.
   :URl: http://ip_servidor/pedido/cadastrar/
    '''
    template_name_json = 'includes/form_json.html'
    template_mensagem = 'includes/mensagem_json.html'

    def get(self, request, id, **kwargs):
        '''
        :param request:
        :param id:
        :param kwargs: id do produto
        :return: HTML do modal
        '''
        context = {}
        data = {}
        form = PedidoForm(id=id)
        context['form'] = form
        context['titulo'] = get_object_or_404(Produto, pk=self.kwargs.get("id"))
        context['url'] = reverse('pedido-produto-add', kwargs={"id": id})

        data['html_form'] = render_to_string(self.template_name_json, context, request=request)
        return JsonResponse(data)

    def post(self, request, id):
        '''
        :param self:
        :param request:
        :param id: id do produto
        :return: sucesso ou não do cadstro
        '''
        form = PedidoForm(request.POST, request.FILES, id=id)
        if form.is_valid():
            form = form.save(commit=False)
            form.cliente = get_object_or_404(Cliente, username=request.user)
            form.produto = get_object_or_404(Produto, id=id)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        '''
        :param self:
        :param form: de cadastro do pedido
        :return: um JSON com as informações sobre o cadstro
        '''
        data = dict()
        context = {}
        form.save()
        data['form_is_valid'] = True
        data["carrinho"] = Pedido.objects.filter(desativado=False, finalizado=False,
                                                 cliente__username=self.request.user).count()
        data['html_mensagem'] = render_to_string(self.template_mensagem, context, request=self.request)
        return JsonResponse(data)

    def form_invalid(self, form):
        '''
        :param self:
        :param form: erro no form
        :return: retorna o erro no cadastro
        '''
        context = {}
        data = dict()
        data['form_is_valid'] = False
        context['form'] = form
        context['classe_css'] = 'pedido_add'
        context['titulo'] = get_object_or_404(Produto, pk=self.kwargs.get("id"))
        context["url"] = reverse("pedido-produto-add", kwargs={"id": self.kwargs.get("id")})
        data['html_form'] = render_to_string(self.template_name_json, context, request=self.request)
        return JsonResponse(data)
