# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from marketapp.models.produto import Produto


class ProdutoListView(LoginRequiredMixin, ListView):
    '''
     Lista todos os Produtos.
    :URl: http://ip_servidor/produto/listar/
    '''
    template_name = "marketapp/produto/produto_list.html"
    model = Produto
    queryset = Produto.objects.all()
    paginate_by = 20
