# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import *
from django.contrib.messages.views import SuccessMessageMixin
from marketapp.models.pedido import Pedido


class CarrinhoCheckoutView(LoginRequiredMixin, SuccessMessageMixin, View):
    '''
     Finaliza  o carrinho  do usuário.
    :URl: http://ip_servidor/carrinho/finalizar
    '''
    success_message = "Pedido finalizado com sucesso!"
    queryset = Pedido.objects.all()
    success_url = reverse_lazy('home')

    def get(self, request, *args, **kwargs):
        pedidos = Pedido.objects.filter(desativado=False, finalizado=False, cliente__username=self.request.user)
        for object in pedidos:
            object.finalizado = True
            object.save()
        messages.success(self.request, self.success_message)
        return redirect(reverse('home'))
