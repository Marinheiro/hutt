# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import ugettext_lazy as _
from marketapp.models.pedido import Pedido


class CarrinhoProdutoDeleteView(SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    '''
     Deleta um pedido do carrinho.
    :URl: http://ip_servidor/pedido/<pk>/excluir
    '''

    success_message = _("Pedido  deletado com sucesso!")
    queryset = Pedido.objects.all()
    success_url = reverse_lazy('carrinho-list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.desativado = True
        self.object.save()
        messages.success(self.request, self.success_message)
        return super(CarrinhoProdutoDeleteView, self).form_valid(form)

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def get_success_message(self, cleaned_data):
        return self.success_message % dict(
            cleaned_data,
            name=self.object.title,
        )
