# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from marketapp.models.pedido import Pedido


class CarrinhoListView(LoginRequiredMixin, ListView):
    '''
    Lista todos os Pedido ativos.
    :URl: http://ip_servidor/carrinho/listar/
    '''
    queryset = Pedido.objects.filter()
    template_name = "marketapp/carrinho/carrinho_list.html"

    def get_queryset(self):
        self.queryset = self.queryset.filter(desativado=False, finalizado=False, cliente__username=self.request.user)
        return self.queryset.filter(desativado=False, finalizado=False, cliente__username=self.request.user)

    def get_context_data(self, object_list=None, **kwargs):
        context = super(CarrinhoListView, self).get_context_data(**kwargs)
        pedidos = self.queryset
        context["valor_total"] = sum(valor for valor in [pedido.quantidade * pedido.preco_unit for pedido in pedidos])
        return context
