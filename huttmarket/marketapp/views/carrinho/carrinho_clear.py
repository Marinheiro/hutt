# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import View
from django.contrib.messages.views import SuccessMessageMixin
from marketapp.models.pedido import Pedido


class CarrinhoClearView(LoginRequiredMixin, SuccessMessageMixin, View):
    '''
     Limpa o carrinho do usuário logado.
    :URl: http://ip_servidor/carrinho/limpar
    '''
    success_message = "Carrinho limpo com sucesso!"
    queryset = Pedido.objects.all()
    success_url = reverse_lazy("home")

    def get(self, request, *args, **kwargs):
        pedidos = Pedido.objects.filter(desativado=False, finalizado=False, cliente__username=request.user)
        for object in pedidos:
            object.desativado = True
            object.save()
        messages.success(self.request, self.success_message)
        return redirect(reverse("home"))
