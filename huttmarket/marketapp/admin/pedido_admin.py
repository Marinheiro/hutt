from django.contrib import admin

from marketapp.models.pedido import Pedido


@admin.register(Pedido)
class PedidoAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "produto",
        "cliente",
        "quantidade",
        "preco_unit",
        "desativado",
        "finalizado"
    )
    list_display_links = (
        "id",
        "produto",
        "cliente",
        "quantidade",
        "preco_unit",
    )
    search_fields = (
        "id",
        "produto",
        "cliente",
        "quantidade",
        "preco_unit",
    )
    list_editable = (
        "desativado",
        "finalizado"
    )
    list_filter = (
        ('desativado', admin.BooleanFieldListFilter),
        ('finalizado', admin.BooleanFieldListFilter),
    )
    empty_value_display = '-vazio-'
