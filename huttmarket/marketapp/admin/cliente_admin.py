from django.contrib import admin

from marketapp.models.cliente import Cliente


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "username",
        "first_name",
        "last_name",
        "is_active",
        "is_staff"
    )
    list_display_links = (
        "id",
        "username",
        "first_name",
        "last_name",
    )
    search_fields = (
        "id",
        "username",
        "first_name",
        "last_name",
    )
    list_editable = (
        "is_active",
        "is_staff"
    )
    list_filter = (
        ('is_active', admin.BooleanFieldListFilter),
        ('is_staff', admin.BooleanFieldListFilter),
    )
    empty_value_display = '-vazio-'
