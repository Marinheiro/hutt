from django.contrib import admin

from marketapp.models.produto import Produto


@admin.register(Produto)
class ProdutoAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "nome",
        "preco_unit",
        "multiplo",
        "imagem"
    )
    list_display_links = (
        "id",
        "nome",
        "preco_unit",
        "multiplo",
    )
    search_fields = (
        "id",
        "nome",
        "preco_unit",
        "multiplo",
    )
    empty_value_display = '-vazio-'
