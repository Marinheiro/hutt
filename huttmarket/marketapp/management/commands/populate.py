# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core import management
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Popula o banco com as informações padrão de cliente e de produto'

    def handle(self, *args, **kwargs):
        management.call_command('populate_cliente')
        management.call_command('populate_produto')
        self.stdout.write(self.style.SUCCESS("Runned cliente e produto as success."))
