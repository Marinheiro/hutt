# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
import unicodedata
from csv import DictReader

from django.core.management.base import BaseCommand

from marketapp.models.cliente import Cliente


class Command(BaseCommand):
    help = 'Popula o banco de clientes padrões'

    def handle(self, *args, **kwargs):
        self._create()
        self.stdout.write(self.style.SUCCESS("Runned as success."))

    def _create(self):
        with open('marketapp/management/commands/csv/clientes.csv') as csvfile:
            reader = DictReader(csvfile)
            for row in reader:
                cliente, created = Cliente.objects.get_or_create(
                    username=self.clear_word(row["Nome"].lower()),
                    first_name=row["Nome"].split(" ")[0] if " " in row["Nome"] else row["Nome"],
                    last_name=row["Nome"].split(" ")[1] if " " in row["Nome"] else row["Nome"],
                    is_staff=True,
                    is_active=True,
                )
                cliente.set_password(self.clear_word(row["Nome"].lower()))
                cliente.save()
                if created:
                    self.stdout.write(self.style.SUCCESS(
                        "{} {} {} - created with success".format(row['ID'], row['Nome'],
                                                                 self.clear_word(row["Nome"].lower()))))
                else:
                    self.stdout.write(self.style.NOTICE(
                        "{} {} {} - created with success".format(row['ID'], row['Nome'],
                                                                 self.clear_word(row["Nome"].lower()))))

    @staticmethod
    def clear_word(word):
        nfkd = unicodedata.normalize('NFKD', word)
        word = u"".join([c for c in nfkd if not unicodedata.combining(c)])
        word = re.sub('[^a-zA-Z0-9 \\\]', '', word)
        return re.sub("\s+", "", word)
