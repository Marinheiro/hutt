# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management.base import BaseCommand
from csv import DictReader

from marketapp.models.produto import Produto


class Command(BaseCommand):
    help = 'Popula o banco de produtos padrões'

    def handle(self, *args, **kwargs):
        self._create()
        self.stdout.write(self.style.SUCCESS("Runned as success."))

    def _create(self):
        with open('marketapp/management/commands/csv/produtos.csv') as csvfile:
            reader = DictReader(csvfile)
            for row in reader:
                produto, created = Produto.objects.get_or_create(
                    nome=row["Nome"],
                    preco_unit=float(row["Preço Unitário"]),
                    multiplo=int(row["Múltiplo"]) if row["Múltiplo"] != '' else 1
                )
                produto.save()
                if created:
                    self.stdout.write(self.style.SUCCESS(
                        "{} {} {} {} - created with success".format(row['ID'],
                                                                    row['Nome'],
                                                                    row["Preço Unitário"],
                                                                    row["Múltiplo"])))
                else:
                    self.stdout.write(self.style.NOTICE(
                        "{} {} {} {} - atualizado with success".format(row['ID'],
                                                                    row['Nome'],
                                                                    row["Preço Unitário"],
                                                                    row["Múltiplo"])))
