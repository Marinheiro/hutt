# -*- coding: utf-8 -*-
from marketapp.tests.views.cliente.cliente_view_create_test import *
from marketapp.tests.views.produto.produto_view_list_test import *
from marketapp.tests.views.pedido.pedido_view_list_test import *
from marketapp.tests.views.pedido.pedido_view_checkout_test import *
from marketapp.tests.views.pedido.pedido_view_create_test import *
from marketapp.tests.views.pedido.pedido_view_update_test import *
from marketapp.tests.views.carrinho.carrinho_view_list_test import *
from marketapp.tests.views.carrinho.carrinho_view_clear_test import *
from marketapp.tests.views.carrinho.carrinho_view_excluir_test import *
from marketapp.tests.views.carrinho.carrinho_view_checkout_test import *

from marketapp.tests.forms.cliente_form_test import *
from marketapp.tests.forms.pedido_form_test import *
from marketapp.tests.forms.produto_form_test import *
from marketapp.tests.models.cliente_model_test import *
from marketapp.tests.models.pedido_model_test import *
from marketapp.tests.models.produto_model_test import *

