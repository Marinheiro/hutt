# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from marketapp.forms.cliente import ClienteForm


class ClienteFormTest(TestCase):
    """Teste de Formulário de Cliente"""

    def test_valid_form(self):
        """Teste de formulário válido de Cliente"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'solo1@gmail.com',
                'password': '123456789',
                'password_checker': '123456789',
                'username': 'hansolo2',
                }

        form = ClienteForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form_sem_username(self):
        """Teste de formulário válido de sem username"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'solo1@gmail.com',
                'password': '123456789',
                'password_checker': '123456789',
                'username': '',
                }

        form = ClienteForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_email(self):
        """Teste de formulário válido de sem email"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'',
                'password': '123456789',
                'password_checker': '123456789',
                'username': 'hansolo',
                }

        form = ClienteForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_first_name(self):
        """Teste de formulário válido de sem first_name"""
        data = {'first_name': u'',
                'last_name': u'Solo',
                'email': u'solos@gmail.com',
                'password': '123456789',
                'password_checker': '123456789',
                'username': 'hansolo',
                }

        form = ClienteForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_last_name(self):
        """Teste de formulário válido de sem last_name"""
        data = {'first_name': u'Han',
                'last_name': u'',
                'email': u'solos@gmail.com',
                'password': '123456789',
                'password_checker': '123456789',
                'username': 'hansolo',
                }

        form = ClienteForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_password(self):
        """Teste de formulário válido de sem password"""
        data = {'first_name': u'Han',
                'last_name': u'',
                'email': u'solos@gmail.com',
                'password': '',
                'password_checker': '',
                'username': 'hansolo',
                }

        form = ClienteForm(data=data)
        self.assertFalse(form.is_valid())
