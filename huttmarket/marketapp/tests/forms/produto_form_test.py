# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from marketapp.forms.produto import ProdutoForm


class ProdutoFormTest(TestCase):
    """Teste de Formulário de Produto"""

    def test_valid_form(self):
        """Teste de formulário válido de Produto"""
        data = {'nome': u'Super Star Destroyer',
                'preco_unit': 4570000.00,
                'multiplo': 1,
                'imagem': '',
                }

        form = ProdutoForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form_sem_nome(self):
        """Teste de formulário válido de sem nome"""
        data = {'nome': u'',
                'preco_unit': 4570000.00,
                'multiplo': 1,
                'imagem': '',
                }

        form = ProdutoForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_preco(self):
        """Teste de formulário válido de sem preco"""
        data = {'nome': u'Super Star Destroyer',
                'preco_unit': None,
                'multiplo': 1,
                'imagem': '',
                }

        form = ProdutoForm(data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_multiplo(self):
        """Teste de formulário válido de sem multiplo"""
        data = {'nome': u'Super Star Destroyer',
                'preco_unit': 4570000.00,
                'multiplo': None,
                'imagem': '',
                }

        form = ProdutoForm(data=data)
        self.assertFalse(form.is_valid())
