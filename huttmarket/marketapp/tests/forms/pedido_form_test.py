# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from marketapp.forms.pedido import PedidoForm
from marketapp.models.produto import Produto


class PedidoFormTest(TestCase):
    """Teste de Formulário de Pedido"""

    def create_produto(self, nome=u'Super Star Destroyer', preco_unit=4570000.00, multiplo=1, imagem=''):
        """Método que cria produto"""
        return Produto.objects.create(nome=nome,
                                      preco_unit=preco_unit,
                                      multiplo=multiplo,
                                      imagem=imagem)

    def test_valid_form(self):
        """Teste de formulário válido de Pedido"""
        produto = self.create_produto()
        data = {'quantidade': 1,
                'preco_unit': 4570000.00,
                }

        form = PedidoForm(id=produto.id, data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form_sem_quantidade(self):
        """Teste de formulário válido de sem quantidade"""
        produto = self.create_produto()
        data = {'quantidade': None,
                'preco_unit': 4570000.00,
                }

        form = PedidoForm(id=produto.id, data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_sem_preco_unit(self):
        """Teste de formulário válido de sem preco_unit"""
        produto = self.create_produto()
        data = {'quantidade': 1,
                'preco_unit': None,
                }

        form = PedidoForm(id=produto.id, data=data)
        self.assertFalse(form.is_valid())
