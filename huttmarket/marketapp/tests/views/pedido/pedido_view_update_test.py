# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.urls import reverse


class PedidoUpdateViewTest(TestCase):
    """Teste de view de editar o pedido"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='darthvader',
                                          password='darthvader')

    def test_pedido_checkout_success(self):
        """Método que testa a edição do pedido"""
        data = {'quantidade': 10,
                'preco_unit': 4570001.00,
                }

        self.response = self.client.get(reverse('carrinho-list'))
        self.assertEqual(len(self.response.context[-1]['object_list']), 1, "Deve conter 1 elementos")
        pedido_anterior = self.response.context[-1]['object_list'][0]
        self.assertEqual(pedido_anterior.quantidade, 1)
        self.assertEqual(pedido_anterior.preco_unit, 4570000.00)

        url = reverse('pedido-editar', kwargs={"id": 20})
        self.response = self.client.get(url)
        self.assertTrue(200, self.response.status_code)
        self.response = self.client.post(url, data=data)

        self.response = self.client.get(reverse('carrinho-list'))
        self.assertEqual(len(self.response.context[-1]['object_list']), 1, "Deve conter 1 elementos")
        pedido_atual = self.response.context[-1]['object_list'][0]

        self.assertEqual(pedido_atual.quantidade, 10)
        self.assertEqual(pedido_atual.preco_unit, 4570001.00)
