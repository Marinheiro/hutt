# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse


class PedidoListViewTest(TestCase):
    """Teste de view de listagem de pedidos"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='admin',
                                          password='admin')

    def test_pedido_list_success(self):
        """Método que testa a listagem de pedidos"""
        self.response = self.client.get(reverse('pedido-list'))
        self.assertTrue(200, self.response.status_code)
        self.assertEqual(len(self.response.context[-1]['object_list']), 5, "Deve conter 5 elementos")
        self.assertTemplateUsed(self.response, 'marketapp/pedido/pedido_list_admin.html')
