# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse


class PedidoCreateViewTest(TestCase):
    """Teste de view de criar pedido"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='darthvader',
                                          password='darthvader')

    def test_pedido_checkout_success(self):
        """Método que testa criar pedido"""
        data = {'quantidade': 5,
                'preco_unit': 6000.00,
                }
        url = reverse('pedido-produto-add', kwargs={"id": 17})
        self.response = self.client.get(url)
        self.assertTrue(200, self.response.status_code)
        self.response = self.client.post(url, data=data)
        self.response = self.client.get(reverse('carrinho-list'))
        self.assertEqual(len(self.response.context[-1]['object_list']), 2, "Deve conter 2 elementos")
