# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse


class CarrinhoCheckoutViewTest(TestCase):
    """Teste de view de checkout de pedidos do carrinho"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='darthvader',
                                          password='darthvader')

    def test_carrinho_checkout_success(self):
        """Método que testa o checkout de pedidos do carrinho"""
        self.response = self.client.get(reverse('carrinho-finalizar'))
        self.assertTrue(200, self.response.status_code)
        self.response = self.client.get(reverse('carrinho-list'))
        self.assertEqual(len(self.response.context[-1]['object_list']), 0, "Deve conter nenhum elemento")
