# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse


class CarrinhoExcluirViewTest(TestCase):
    """Teste de view de exclusão de pedidos do carrinho"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='darthvader',
                                          password='darthvader')

    def test_carrinho_excluir_success(self):
        """Método que testa a exclusão de pedidos do carrinho"""
        self.response = self.client.get(reverse('carrinho-produto-excluir', kwargs={"pk": 20}))
        self.assertTrue(200, self.response.status_code)
        self.response = self.client.get(reverse('carrinho-list'))
        self.assertEqual(len(self.response.context[-1]['object_list']), 0, "Deve conter nenhum elemento")
