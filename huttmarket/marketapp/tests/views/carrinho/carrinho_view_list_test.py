# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse


class CarrinhoListViewTest(TestCase):
    """Teste de view de listagem de pedidos do carrinho"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='darthvader',
                                          password='darthvader')

    def test_carrinho_list_success(self):
        """Método que testa a listagem de pedidos do carrinho"""
        self.response = self.client.get(reverse('carrinho-list'))
        self.assertTrue(200, self.response.status_code)
        self.assertEqual(len(self.response.context[-1]['object_list']), 1, "Deve conter 1 elemento")
        self.assertTemplateUsed(self.response, 'marketapp/carrinho/carrinho_list.html')
