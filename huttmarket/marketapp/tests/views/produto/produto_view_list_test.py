# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse


class ProdutoListViewTest(TestCase):
    """Teste de viee de istagem de produtos"""

    fixtures = ['users.json', 'marketapp.json']

    def setUp(self):
        self.response = self.client.login(username='hansolo',
                                          password='hansolo')

    def test_produto_list_success(self):
        """Método que testa a listagem de produtos"""
        self.response = self.client.get(reverse('home'))
        self.assertTrue(200, self.response.status_code)
        self.assertEqual(len(self.response.context[-1]['object_list']), 7, "Deve conter 7 elementos")
        self.assertTemplateUsed(self.response, 'marketapp/produto/produto_list.html')
