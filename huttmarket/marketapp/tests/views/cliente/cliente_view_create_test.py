# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse
from marketapp.models.cliente import Cliente
from django.contrib.auth.hashers import make_password


class ClienteCreateViewTest(TestCase):
    """Teste de viee de criação de cliente"""

    def create_cliente(self,
                       first_name=u'Han',
                       last_name=u'Solo',
                       email=u'han_solo@gmail.com',
                       password=make_password(None),
                       username='solo'):
        """Método que cria cliente"""
        return Cliente.objects.create(first_name=first_name,
                                      last_name=last_name,
                                      email=email,
                                      password=password,
                                      username=username)

    def test_cliente_create_success(self):
        """Método que testa a criação de cliente"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'solo1@gmail.com',
                'password': '123456789',
                'password_checker': '123456789',
                'username': 'hansolo',
                }
        url = reverse("signup")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302, "O Status deve ser 302")

    def test_cliente_create_fail_password(self):
        """Método que testa a criação de cliente com password diferente"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'solo1@gmail.com',
                'password': '567890',
                'password_checker': '123456',
                'username': 'hansolo',
                }
        url = reverse("signup")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
        response = self.client.post(url, data=data)
        self.assertIn(b"Senhas diferentes ou possui menos de cinco digitos!", response.content, "Senha deve ser igual")
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")

    def test_cliente_create_sem_username(self):
        """Método que testa a criação de cliente sem username"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'solo1@gmail.com',
                'password': '123456',
                'password_checker': '123456',
                'username': '',
                }
        url = reverse("signup")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
        response = self.client.post(url, data=data)
        self.assertIn(b"This field is required.", response.content, " O Username deve ser preenchido")
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")

    def test_cliente_create_sem_first_name(self):
        """Método que testa a criação de cliente sem first_name"""
        data = {'first_name': u'',
                'last_name': u'Solo',
                'email': u'solo1@gmail.com',
                'password': '123456',
                'password_checker': '123456',
                'username': 'hansolo',
                }
        url = reverse("signup")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
        response = self.client.post(url, data=data)
        self.assertIn(b"This field is required.", response.content, " O first_name deve ser preenchido")
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")

    def test_cliente_create_sem_last_name(self):
        """Método que testa a criação de cliente sem last_name"""
        data = {'first_name': u'Han',
                'last_name': u'',
                'email': u'solo1@gmail.com',
                'password': '123456',
                'password_checker': '123456',
                'username': 'hansolo',
                }
        url = reverse("signup")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
        response = self.client.post(url, data=data)
        self.assertIn(b"This field is required.", response.content, " O last_name deve ser preenchido")
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")

    def test_cliente_create_sem_email(self):
        """Método que testa a criação de cliente sem email"""
        data = {'first_name': u'Han',
                'last_name': u'Solo',
                'email': u'',
                'password': '123456',
                'password_checker': '123456',
                'username': 'hansolo',
                }
        url = reverse("signup")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
        response = self.client.post(url, data=data)
        self.assertIn(b"This field is required.", response.content, " O email deve ser preenchido")
        self.assertEqual(response.status_code, 200, "O Status deve ser 200")
