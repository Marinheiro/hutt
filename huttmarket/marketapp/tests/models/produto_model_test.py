# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from marketapp.models.produto import Produto


class ProdutoModelTest(TestCase):
    """Teste de Model de Produto"""

    def create_produto(self, nome=u'Super Star Destroyer', preco_unit=4570000.00, multiplo=1, imagem=''):
        """Método que cria produto"""
        return Produto.objects.create(nome=nome,
                                      preco_unit=preco_unit,
                                      multiplo=multiplo,
                                      imagem=imagem)

    def test_produto_creation(self):
        """Método que testa a criação de produto"""
        produto = self.create_produto()
        self.assertTrue(isinstance(produto, Produto))
        self.assertEqual(produto.__unicode__(), produto.nome)
