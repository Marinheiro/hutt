# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.hashers import make_password
from django.test import TestCase

from marketapp.models.cliente import Cliente
from marketapp.models.pedido import Pedido
from marketapp.models.produto import Produto


class PedidoModelTest(TestCase):
    """Teste de Model de Pedido"""

    def create_produto(self, nome=u'Super Star Destroyer', preco_unit=4570000.00, multiplo=1, imagem=''):
        """Método que cria produto"""
        return Produto.objects.create(nome=nome,
                                      preco_unit=preco_unit,
                                      multiplo=multiplo,
                                      imagem=imagem)

    def create_pedido(self,
                      quantidade=1,
                      preco_unit=4570000.00,
                      desativado=False,
                      finalizado=False):
        """Método que cria pedido"""
        return Pedido.objects.create(produto=self.create_produto(),
                                     cliente=self.create_cliente(),
                                     quantidade=quantidade,
                                     preco_unit=preco_unit,
                                     desativado=desativado,
                                     finalizado=finalizado)

    def create_cliente(self,
                       first_name=u'Han',
                       last_name=u'Solo',
                       email=u'han_solo@gmail.com',
                       password=make_password(None),
                       username='solo'):
        """Método que cria cliente"""
        return Cliente.objects.create(first_name=first_name,
                                      last_name=last_name,
                                      email=email,
                                      password=password,
                                      username=username)

    def test_pedido_creation(self):
        """Método que testa a criação de pedido"""
        pedido = self.create_pedido()
        self.assertTrue(isinstance(pedido, Pedido))
        self.assertEqual(pedido.__unicode__(), "{}".format(pedido.pk))
