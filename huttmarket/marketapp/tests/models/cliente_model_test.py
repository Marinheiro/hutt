# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.hashers import make_password
from django.test import TestCase
from marketapp.models.cliente import Cliente


class ClienteModelTest(TestCase):
    """Teste de Model de Cliente"""

    def create_cliente(self,
                       first_name=u'Han',
                       last_name=u'Solo',
                       email=u'han_solo@gmail.com',
                       password=make_password('1234'),
                       username='solo'):
        """Método que cria cliente"""
        return Cliente.objects.create(first_name=first_name,
                                      last_name=last_name,
                                      email=email,
                                      password=password,
                                      username=username)

    def test_cliente_creation(self):
        """Método que testa a criação de cliente"""
        cliente = self.create_cliente()
        self.assertTrue(isinstance(cliente, Cliente))
        self.assertEqual(cliente.__unicode__(), cliente.username)
