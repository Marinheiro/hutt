# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.urls import reverse_lazy


class Produto(models.Model):
    """
    :param nome: models.CharField(max_length=250)
    :param preco_unit: models.DecimalField(max_digits=10, decimal_places=2)
    :param multiplo: models.PositiveIntegerField(default=1)
    :param imagem: models.ImageField(upload_to="produto", blank=True)
    """

    nome = models.CharField(max_length=250)
    preco_unit = models.DecimalField(max_digits=10, decimal_places=2)
    multiplo = models.PositiveIntegerField(default=1)
    imagem = models.ImageField(upload_to="produto", blank=True)

    class Meta:
        verbose_name_plural = 'Produtos'
        ordering = ["nome"]

    def get_absolute_url(self):
        """
        :return: Define a url de retorno após executar os metodos de Create ou Update
        """
        return reverse_lazy('produto-list')

    def __str__(self):
        """
        :return: Define nome de exibição para o objeto
        """
        return '%s' % self.nome

    def __unicode__(self):
        """
        :return: Define nome o unicode de exibição para o objeto
        """
        return self.nome
