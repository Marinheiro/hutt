# -*- coding: utf-8 -*-

from django.conf.urls import url

from marketapp.views.cliente.cliente_create import ClienteCreateView

from marketapp.views.carrinho.carrinho_checkout import CarrinhoCheckoutView
from marketapp.views.carrinho.carrinho_clear import CarrinhoClearView
from marketapp.views.carrinho.carrinho_delete import CarrinhoProdutoDeleteView
from marketapp.views.carrinho.carrinho_list import CarrinhoListView

from marketapp.views.pedido.pedido_checkout import PedidoCheckoutView
from marketapp.views.pedido.pedido_create import AjaxPedidoCreateView
from marketapp.views.pedido.pedido_edit import AjaxPedidoUpdateView
from marketapp.views.pedido.pedido_list import PedidoListView

from marketapp.views.produto.produto_list import ProdutoListView

urlpatterns = [
    # produto
    url(r'^$', ProdutoListView.as_view(), name='home'),

    # Pedido
    url(r'pedido/listar/$', PedidoListView.as_view(), name='pedido-list'),
    url(r'produto/(?P<id>[\d\-]+)/comprar/$', PedidoCheckoutView.as_view(), name='produto-comprar'),
    url(r'pedido/produto/(?P<id>[\d\-]+)/$', AjaxPedidoCreateView.as_view(), name='pedido-produto-add'),
    url(r'pedido/(?P<id>[\d\-]+)/editar$', AjaxPedidoUpdateView.as_view(), name='pedido-editar'),

    # Carrinho
    url(r'carrinho/$', CarrinhoListView.as_view(), name='carrinho-list'),
    url(r'carrinho/produto/(?P<pk>[\d\-]+)/excluir$', CarrinhoProdutoDeleteView.as_view(), name='carrinho-produto-excluir'),
    url(r'carrinho/finalizar/$', CarrinhoCheckoutView.as_view(), name='carrinho-finalizar'),
    url(r'carrinho/limpar/$', CarrinhoClearView.as_view(), name='carrinho-limpar'),

    # Cliente
    url(r'usuario/cadastrar/$', ClienteCreateView.as_view(), name='signup'),
]
