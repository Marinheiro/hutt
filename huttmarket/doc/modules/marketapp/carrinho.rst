Carrinho
========

Views
-----

.. automodule:: marketapp.views.carrinho.carrinho_checkout

.. automodule:: marketapp.views.carrinho.carrinho_clear

.. automodule:: marketapp.views.carrinho.carrinho_delete

.. automodule:: marketapp.views.carrinho.carrinho_list

