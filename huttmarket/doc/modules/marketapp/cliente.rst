Cliente
=======

Views
-----
.. automodule:: marketapp.views.cliente.cliente_create


Model
-----
.. automodule:: marketapp.models.cliente
