Pedido
======

Views
-----
.. automodule:: marketapp.views.pedido.pedido_checkout

.. automodule:: marketapp.views.pedido.pedido_create

.. automodule:: marketapp.views.pedido.pedido_edit

.. automodule:: marketapp.views.pedido.pedido_list

Model
-----
.. automodule:: marketapp.models.pedido
