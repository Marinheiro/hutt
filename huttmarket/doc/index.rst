Bem vindos ao Hutt Market!
=================================



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`





Conteudo:
=========
.. toctree::
  :maxdepth: 2


  modules/marketapp/carrinho.rst
  modules/marketapp/pedido.rst
  modules/marketapp/cliente.rst
  modules/marketapp/produto.rst
