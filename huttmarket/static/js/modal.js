$(document).ready(function () {

    function loadForm() {
        var btn = $(this);
        $.ajax({
            url: btn.attr("data-url"),
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $("#modal-object").html("");
            },
            success: function (data) {
                console.log(data);
                $("#modal-object").html(data.html_form);
                $("#modal-object").modal("show");
            }
        });
    }

    function saveForm() {
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {

                if (data.form_is_valid) {
                    $("#modal-object").modal("hide");
                    $("#mensagem").html(data.html_mensagem);
                    if (data.editado) {
                        $("#pedido_" + data.pedido + "_quantidade").text(data.quantidade);
                        $("#pedido_" + data.pedido + "_preco_produto").text("R$ " + data.preco);
                        $("#pedido_" + data.pedido + "_valor_por_produto").text("R$ " + (data.preco * data.quantidade).toFixed(2));
                        $("#pedido_" + data.pedido + "_rentabilidade").html(rentabilidade(data));
                        $("#valor_total").html(data.total);
                    }
                    if (data.carrinho) {
                        $("#carrinho").text(data.carrinho)
                    }
                } else {
                    $("#modal-object").modal("hide");
                    $("#modal-object").html(data.html_form);
                    $("#modal-object").modal("show");
                }
            }
        });
        return false;
    }

    function rentabilidade(data){
        if (data.preco > data.preco_antigo){
            return "<div class='ui medium'><i class='caret square up blue icon'></i> Ótima</div>"
        }
        return "<div class='ui medium'><i class='caret square up teal icon'></i> Bom</div>"
    }

    $(".js-create-object").click(loadForm);
    $("#modal-object").on("submit", ".js-object-create-form", saveForm);

});